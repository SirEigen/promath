% Copyright (C) 2013, Cameron White
% All rights reserved.
% 
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions
% are met:
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
% 3. Neither the name of the project nor the names of its contributors
%    may be used to endorse or promote products derived from this software
%    without specific prior written permission.
% 
% THIS SOFTWARE IS PROVIDED BY THE PROJECT AND CONTRIBUTORS ``AS IS'' AND
% ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED.  IN NO EVENT SHALL THE PROJECT OR CONTRIBUTORS BE LIABLE
% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
% OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
% HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
% LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
% OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
% SUCH DAMAGE.

vget([X|_], 0, X).
vget([_|T], I, X) :-
	I > 0,
	I1 is I-1,
	vget(T, I1, X).

vadd([], [], []).
vadd([H1|T1], [H2|T2], [H3|T3]) :-
	H3 is H1 + H2,
	vadd(T1, T2, T3).

vsub([], [], []).
vsub([H1|T1], [H2|T2], [H3|T3]) :-
	H3 is H1 - H2,
	vsub(T1, T2, T3).

vscale([], _, []).
vscale([H1|T1], X, [H2|T2]) :-
	H2 is X * H1,
	vscale(T1, X, T2). 

dot([], [], 0).
dot([H1|T1], [H2|T2], X) :-
	dot(T1, T2, X0),
	X is H1 * H2 + X0.

magnitude([], 0).
magnitude(V, X) :-
    dot(V, V, X0),
    X is sqrt(X0).

normalize([], 0).
normalize(V, X) :-
    magnitude(V, M),
    N is 1 / M,
    vscale(V, N, X).

removeElement([_|T1], 0, T1).
removeElement([H1|T1], I, [H2|T2]) :-
	I > 0,
	I1 is I - 1,
	H2 is H1,
	removeElement(T1, I1, T2).
